To run the application, you need to install Java 21 or higher. You can download it from https://www.oracle.com/java/technologies/java-se-glance.html.

You need Node.js and the npm package manager to run the application. You can download them from https://nodejs.org/en/.

To run the application, you need to install MongoDB. You can install it from https://www.mongodb.com/try/download/community.
    
Create your credentials for the database, then create the database itself. Fill in the credentials in shift-scheduler/src/main/resources/application.yaml.
    
To start the backend, run shift-scheduler/src/main/java/cz/cvut/fel/shiftscheduler/ShiftSchedulerApplication.java.
    
Run npm install -q @angular/cli in the frontend folder.
    
Run ng build to start the frontend.
