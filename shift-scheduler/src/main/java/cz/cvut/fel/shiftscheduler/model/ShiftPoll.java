package cz.cvut.fel.shiftscheduler.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.util.ProxyUtils;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class ShiftPoll {

    @Id
    @GeneratedValue
    String id;
    @OneToOne
    Shift shift;
    @ManyToOne
    Account account;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ProxyUtils.getUserClass(this) != ProxyUtils.getUserClass(o))
            return false;
        ShiftPoll shiftPoll = (ShiftPoll) o;
        return id != null && Objects.equals(id, shiftPoll.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
