package cz.cvut.fel.shiftscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class ShiftPollDto {
    String id;
    ShiftDto shift;
    AccountDto account;
}
