package cz.cvut.fel.shiftscheduler.calculation;

import com.gurobi.gurobi.*;
import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.dto.ShiftDto;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

public class GurobiCalculation {

    public static boolean[][] calculate(List<Integer> capacities, List<Integer> lengths, List<List<AccountDto>> shiftAccounts,
                                        List<AccountDto> restaurantAccounts, List<ShiftDto> shifts) {
        try {
            GRBEnv env = new GRBEnv(true);
            env.set("logFile", "scheduling.log");
            env.set("OutputFlag", "1");
            env.start();

            GRBModel model = new GRBModel(env);

            // Data: Shifts and Workers
            int numShifts = capacities.size();
            int numWorkers = restaurantAccounts.size();
            double[] hourlyWages = restaurantAccounts.stream()
                    .mapToDouble(account -> {
                        Double salary = account.getSalary();
                        return salary != null ? salary : 9999999;
                    })
                    .toArray();

            int[] shiftHours = lengths.stream()
                    .mapToInt(Integer::intValue)
                    .toArray();

            int[] shiftCapacities = capacities.stream()
                    .mapToInt(Integer::intValue)
                    .toArray();

            int[] shiftStarts = shifts.stream()
                    .map(shift -> (int) Duration.between(LocalDateTime.of(2024, 1, 1, 0, 0), shift.getFrom()).toHours())
                    .mapToInt(Integer::intValue)
                    .toArray();

            int[] shiftEnds = new int[shifts.size()];
            for (int i = 0; i < shifts.size(); i++) {
                shiftEnds[i] = shiftStarts[i] + lengths.get(i);
            }

            boolean[][] workerAvailability = new boolean[numWorkers][numShifts];
            boolean result = false;
            boolean[][] resultMatrix = new boolean[numWorkers][numShifts];
            for (int i = 0; i < numWorkers; i++) {
                AccountDto currentWorker = restaurantAccounts.get(i);
                for (int j = 0; j < numShifts; j++) {
                    workerAvailability[i][j] = shiftAccounts.get(j).stream()
                            .anyMatch(account -> account.getId().equals(currentWorker.getId()));
                }
            }

            // Decision variables
            GRBVar[][] x = new GRBVar[numWorkers][numShifts];
            for (int i = 0; i < numWorkers; i++) {
                for (int j = 0; j < numShifts; j++) {
                    if (workerAvailability[i][j]) {
                        x[i][j] = model.addVar(0, 1, hourlyWages[i] * shiftHours[j], GRB.BINARY, "x_" + i + "_" + j);
                    }
                }
            }

            // Objective: Minimize total cost
            GRBLinExpr objective = new GRBLinExpr();
            for (int i = 0; i < numWorkers; i++) {
                for (int j = 0; j < numShifts; j++) {
                    if (workerAvailability[i][j]) {
                        objective.addTerm(hourlyWages[i] * shiftHours[j], x[i][j]);
                    }
                }
            }
            model.setObjective(objective, GRB.MINIMIZE);

            // Constraints: Shift capacity - Ensure exactly the capacity
            for (int j = 0; j < numShifts; j++) {
                GRBLinExpr capacityConstraint = new GRBLinExpr();
                for (int i = 0; i < numWorkers; i++) {
                    if (workerAvailability[i][j]) {
                        capacityConstraint.addTerm(1, x[i][j]);
                    }
                }
                model.addConstr(capacityConstraint, GRB.EQUAL, shiftCapacities[j], "cap_" + j);
            }

            // Constraint: No overlapping shifts for any worker
            for (int i = 0; i < numWorkers; i++) {
                for (int j = 0; j < numShifts; j++) {
                    for (int k = j + 1; k < numShifts; k++) {
                        if ((shiftStarts[j] < shiftEnds[k] && shiftStarts[k] < shiftEnds[j]) && workerAvailability[i][j] && workerAvailability[i][k]) {
                            GRBLinExpr overlapExpr = new GRBLinExpr();
                            overlapExpr.addTerm(1.0, x[i][j]);
                            overlapExpr.addTerm(1.0, x[i][k]);
                            model.addConstr(overlapExpr, GRB.LESS_EQUAL, 1, "no_overlap_" + i + "_" + j + "_" + k);
                        }
                    }
                }
            }

            // Optimize model
            model.optimize();

            // Output solution
            for (int i = 0; i < numWorkers; i++) {
                for (int j = 0; j < numShifts; j++) {
                    if (workerAvailability[i][j] && x[i][j].get(GRB.DoubleAttr.X) == 1.0) {
                        result = true;
                        resultMatrix[i][j] = true;
                        System.out.println("Worker " + i + " is assigned to Shift " + j);
                    }
                }
            }


            // Dispose of model and environment
            model.dispose();
            env.dispose();
            if (result) return resultMatrix;
            else return null;
        } catch (GRBException e) {
            System.out.println("Error code: " + e.getErrorCode() + ". " + e.getMessage());
            return null;
        }
    }
}
