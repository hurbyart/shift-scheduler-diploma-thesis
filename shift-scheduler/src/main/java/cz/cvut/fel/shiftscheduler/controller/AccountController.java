package cz.cvut.fel.shiftscheduler.controller;

import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.service.AccountService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@RequestMapping("/account")
public class AccountController {

    AccountService accountService;

    @GetMapping
    public List<AccountDto> getAll() {
        return accountService.getAll();
    }

    @GetMapping("/{accountId}")
    public AccountDto getById(@PathVariable String accountId) {
        return accountService.getById(accountId);
    }

    @GetMapping("/restaurant")
    public List<AccountDto> getByRestaurant() {
        return accountService.getByRestaurant();
    }

    @GetMapping("/current")
    public AccountDto getCurrent() {
        return accountService.getCurrent();
    }

    @PostMapping
    public void create(@RequestBody AccountDto accountDto) {
        accountService.create(accountDto);
    }

    @PutMapping
    public AccountDto update(@RequestBody AccountDto accountDto) {
        return accountService.update(accountDto);
    }

    @DeleteMapping("/{accountId}")
    public void delete(@PathVariable String accountId) {
        accountService.delete(accountId);
    }
}
