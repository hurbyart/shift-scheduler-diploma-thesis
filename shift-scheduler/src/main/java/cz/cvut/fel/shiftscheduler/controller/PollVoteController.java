package cz.cvut.fel.shiftscheduler.controller;

import cz.cvut.fel.shiftscheduler.dto.PollVoteDto;
import cz.cvut.fel.shiftscheduler.dto.ShiftWithAnswerDto;
import cz.cvut.fel.shiftscheduler.service.PollVoteService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@RequestMapping("/shift/poll/vote")
public class PollVoteController {

    PollVoteService pollVoteService;

    @GetMapping("/{pollVoteId}")
    public PollVoteDto getById(@PathVariable String pollVoteId) {
        return pollVoteService.getById(pollVoteId);
    }

    @GetMapping("/shiftPollId")
    public List<PollVoteDto> getByShiftPoll(@RequestParam String shiftPollId) {
        return pollVoteService.getByShiftPoll(shiftPollId);
    }

    @GetMapping
    public List<ShiftWithAnswerDto> getByAllWithShift() {
        return pollVoteService.getAllShiftWithAnswer();
    }

    @PostMapping
    public void create(@RequestBody PollVoteDto pollVoteDto) {
        pollVoteService.create(pollVoteDto);
    }

    @PutMapping
    public PollVoteDto update(@RequestBody PollVoteDto pollVoteDto) {
        return pollVoteService.update(pollVoteDto);
    }

    @DeleteMapping("/{pollVoteId}")
    public void delete(@PathVariable String pollVoteId) {
        pollVoteService.delete(pollVoteId);
    }
}
