package cz.cvut.fel.shiftscheduler.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.util.ProxyUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class Account implements UserDetails {

    @Id
    @GeneratedValue
    String id;
    String username;
    String password;
    String firstName;
    String secondName;
    String position;
    LocalDate birthDate;
    String photo;
    String phoneNumber;
    Double salary;
    @Enumerated(EnumType.STRING)
    private Role role;
    @ManyToOne
    Restaurant restaurant;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(role);
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ProxyUtils.getUserClass(this) != ProxyUtils.getUserClass(o))
            return false;
        Account account = (Account) o;
        return id != null && Objects.equals(id, account.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
