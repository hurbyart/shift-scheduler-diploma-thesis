package cz.cvut.fel.shiftscheduler.controller;

import cz.cvut.fel.shiftscheduler.dto.ScheduleDto;
import cz.cvut.fel.shiftscheduler.service.ScheduleService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@RequestMapping("/schedule")
public class ScheduleController {

    ScheduleService scheduleService;

    @GetMapping
    public List<ScheduleDto> getAll() {
        return scheduleService.getAll();
    }

    @GetMapping("/{scheduleId}")
    public ScheduleDto getById(@PathVariable String scheduleId) {
        return scheduleService.getById(scheduleId);
    }

    @GetMapping("/restaurant")
    public List<ScheduleDto> getByRestaurant(@RequestParam String restaurantId) {
        return scheduleService.getByRestaurant(restaurantId);
    }

    @GetMapping("/shift")
    public ScheduleDto getByShiftId(@RequestParam String shiftId) {
        return scheduleService.findScheduleByShiftId(shiftId);
    }

    @PostMapping
    public void create(@RequestBody ScheduleDto scheduleDto) {
        scheduleService.create(scheduleDto);
    }

    @PutMapping
    public ScheduleDto update(@RequestBody ScheduleDto scheduleDto) {
        return scheduleService.update(scheduleDto);
    }

    @DeleteMapping("/{scheduleId}")
    public void delete(@PathVariable String scheduleId) {
        scheduleService.delete(scheduleId);
    }

    @PostMapping("/calculate/{scheduleId}")
    public ResponseEntity<?> calculateScheduleResult(@PathVariable String scheduleId, @RequestParam Boolean bySalary) {
        return ResponseEntity.ok(scheduleService.calculateScheduleResult(scheduleId, bySalary));
    }
}
