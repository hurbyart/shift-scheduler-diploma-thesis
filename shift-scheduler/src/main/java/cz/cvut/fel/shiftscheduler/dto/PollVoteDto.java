package cz.cvut.fel.shiftscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@AllArgsConstructor
@Builder
@Data
public class PollVoteDto {
    String id;
    Boolean answer;
    ShiftPollDto shiftPollDto;
    AccountDto accountDto;
}

