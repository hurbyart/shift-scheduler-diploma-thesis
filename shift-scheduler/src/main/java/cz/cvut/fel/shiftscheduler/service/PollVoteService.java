package cz.cvut.fel.shiftscheduler.service;

import cz.cvut.fel.shiftscheduler.dto.PollVoteDto;
import cz.cvut.fel.shiftscheduler.dto.ShiftWithAnswerDto;
import cz.cvut.fel.shiftscheduler.model.PollVote;
import cz.cvut.fel.shiftscheduler.model.Schedule;
import cz.cvut.fel.shiftscheduler.model.Shift;
import cz.cvut.fel.shiftscheduler.model.ShiftPoll;
import cz.cvut.fel.shiftscheduler.repository.PollVoteRepository;
import cz.cvut.fel.shiftscheduler.repository.ScheduleRepository;
import cz.cvut.fel.shiftscheduler.repository.ShiftPollRepository;
import cz.cvut.fel.shiftscheduler.repository.ShiftRepository;
import cz.cvut.fel.shiftscheduler.utils.Converter;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class PollVoteService {

    PollVoteRepository pollVoteRepository;
    ShiftPollRepository shiftPollRepository;
    ShiftRepository shiftRepository;
    AccountService accountService;
    ScheduleRepository scheduleRepository;

    public List<PollVoteDto> getAll() {
        return pollVoteRepository.findAll().stream().map(Converter::pollVote).toList();
    }

    public PollVoteDto getById(String id) {
        return Converter.pollVote(pollVoteRepository.findById(id).get());
    }

    public void create(PollVoteDto pollVoteDto) {
        pollVoteDto.setAccountDto(accountService.getCurrent());
        pollVoteRepository.insert(Converter.pollVote(pollVoteDto));
    }

    @Transactional
    public PollVoteDto update(PollVoteDto pollVoteDto) {
        pollVoteRepository.delete(pollVoteRepository.findById(pollVoteDto.getId()).get());
        return Converter.pollVote(pollVoteRepository.insert(Converter.pollVote(pollVoteDto)));
    }

    public void delete(String pollVoteId) {
        pollVoteRepository.delete(pollVoteRepository.findById(pollVoteId).get());
    }

    public List<PollVoteDto> getByShiftPoll(String shiftPollId) {
        return pollVoteRepository.findPollVotesByShiftPoll_Id(shiftPollId).stream()
                .map(Converter::pollVote)
                .toList();
    }

    public List<ShiftWithAnswerDto> getAllShiftWithAnswer() {
        List<ShiftWithAnswerDto> res = new ArrayList<>();
        for(ShiftPoll shiftPoll : shiftPollRepository.findShiftPollsByAccount_Id(accountService.getCurrent().getId())) {
            List<PollVote> pollVotes = pollVoteRepository.findPollVotesByShiftPoll_Id(shiftPoll.getId());
            ShiftWithAnswerDto shiftWithAnswerDto = new ShiftWithAnswerDto();
            Shift shift = shiftRepository.findById(shiftPoll.getShift().getId()).get();
            shiftWithAnswerDto.setShift(Converter.shift(adjustShiftTime(shift)));
            Schedule schedule = scheduleRepository.findById(shift.getSchedule().getId()).get();
            if (!pollVotes.isEmpty()) {
                Boolean lastAnswer = pollVotes.get(pollVotes.size() - 1).getAnswer();
                if(lastAnswer && schedule.getResult() == null) {
                    shiftWithAnswerDto.setAnswer(false);
                } else if (lastAnswer && schedule.getResult().getMap().get(shift.getId()).stream()
                        .filter(acc -> Objects.equals(acc.getId(), accountService.getCurrent().getId())).toList().size() == 1) {
                    shiftWithAnswerDto.setAnswer(true);
                    res.add(shiftWithAnswerDto);
                } else if (schedule.getResult() != null && schedule.getResult().getMap().get(shift.getId()).stream()
                        .filter(acc -> Objects.equals(acc.getId(), accountService.getCurrent().getId())).toList().size() == 0){
                    continue;
                }
            }
            if(!schedule.getIsLocked()) res.add(shiftWithAnswerDto);
        }
        return res;
    }

    private Shift adjustShiftTime(Shift shift) {
        if (shift != null) {
            shift.setFrom(shift.getFrom().minusHours(2));
            shift.setTo(shift.getTo().minusHours(2));
        }
        return shift;
    }
}
