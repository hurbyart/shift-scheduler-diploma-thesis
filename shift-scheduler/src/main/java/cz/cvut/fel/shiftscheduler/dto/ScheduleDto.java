package cz.cvut.fel.shiftscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@Data
public class ScheduleDto {
    String id;
    String name;
    LocalDateTime from;
    LocalDateTime to;
    RestaurantDto restaurant;
    Boolean isLocked;
}
