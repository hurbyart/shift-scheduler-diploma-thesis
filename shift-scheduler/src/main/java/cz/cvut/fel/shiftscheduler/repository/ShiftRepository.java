package cz.cvut.fel.shiftscheduler.repository;

import cz.cvut.fel.shiftscheduler.model.Schedule;
import cz.cvut.fel.shiftscheduler.model.Shift;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShiftRepository extends MongoRepository<Shift, String> {
    List<Shift> findShiftsBySchedule(Schedule schedule);

    List<Shift> findShiftsBySchedule_Id(String id);

    List<Shift> findShiftsBySchedule_RestaurantId(String id);
}
