package cz.cvut.fel.shiftscheduler.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.util.ProxyUtils;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class PollVote {

    @Id
    @GeneratedValue
    String id;
    Boolean answer;
    @OneToOne
    ShiftPoll shiftPoll;
    @OneToOne
    Account account;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ProxyUtils.getUserClass(this) != ProxyUtils.getUserClass(o))
            return false;
        PollVote pollVote = (PollVote) o;
        return id != null && Objects.equals(id, pollVote.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
