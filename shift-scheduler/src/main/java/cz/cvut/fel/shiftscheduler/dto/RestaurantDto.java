package cz.cvut.fel.shiftscheduler.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class RestaurantDto {
    String id;
    String name;
    String location;
}
