package cz.cvut.fel.shiftscheduler.service;

import cz.cvut.fel.shiftscheduler.calculation.GurobiCalculation;
import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.dto.ScheduleDto;
import cz.cvut.fel.shiftscheduler.dto.ShiftDto;
import cz.cvut.fel.shiftscheduler.model.Account;
import cz.cvut.fel.shiftscheduler.model.Result;
import cz.cvut.fel.shiftscheduler.model.Schedule;
import cz.cvut.fel.shiftscheduler.repository.ScheduleRepository;
import cz.cvut.fel.shiftscheduler.utils.Converter;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ScheduleService {

    ScheduleRepository scheduleRepository;
    AccountService accountService;
    ShiftService shiftService;

    public List<ScheduleDto> getAll() {
        return scheduleRepository.findSchedulesByRestaurant_Id(accountService.getCurrent().getRestaurantDto().getId())
                .stream()
                .map(this::adjustScheduleTimeMinus)
                .map(Converter::schedule)
                .toList();
    }

    public ScheduleDto getById(String scheduleId) {
        return Converter.schedule(adjustScheduleTimeMinus(scheduleRepository.findById(scheduleId).get()));
    }

    public List<ScheduleDto> getByRestaurant(String restaurantId) {
        return scheduleRepository.findSchedulesByRestaurant_Id(restaurantId).stream()
                .map(Converter::schedule)
                .toList();
    }

    public void create(ScheduleDto scheduleDto) {
        scheduleDto.setIsLocked(false);
        scheduleDto.setRestaurant(accountService.getCurrent().getRestaurantDto());
        scheduleRepository.insert(adjustScheduleTimePlus(Converter.schedule(scheduleDto)));
    }

    @Transactional
    public ScheduleDto update(ScheduleDto scheduleDto) {
        scheduleDto.setRestaurant(Converter.restaurant(scheduleRepository.findById(scheduleDto.getId()).get().getRestaurant()));
        return Converter.schedule(scheduleRepository.save(adjustScheduleTimePlus(Converter.schedule(scheduleDto))));
    }

    @Transactional
    public void delete(String scheduleId) {
        shiftService.getBySchedule(scheduleId).stream()
                .forEach(shift -> {
                    shiftService.delete(shift.getId());
                });
        scheduleRepository.delete(scheduleRepository.findById(scheduleId).get());
    }

    public ScheduleDto findScheduleByShiftId(String id) {
        return Converter.schedule(adjustScheduleTimeMinus(scheduleRepository.findById(shiftService.getById(id).getScheduleId()).get()));
    }

    public boolean calculateScheduleResult(String id, Boolean bySalary) {
        boolean noSuccess = false;
        if (bySalary) {
            if(!calculateResultSalary(id)) noSuccess = true;
        } else {
            calculateResultQueue(id);
        }
        Schedule schedule = scheduleRepository.findById(id).get();
        schedule.setIsLocked(true);
        scheduleRepository.save(schedule);
        return noSuccess;
    }

    public void calculateResultQueue(String id) {
        List<ShiftDto> shifts = shiftService.getBySchedule(id);
        Map<String, List<Account>> resultMap = new HashMap<>();
        for (ShiftDto shiftDto : shifts) {
            List<AccountDto> accounts = shiftService.getRegisteredAccounts(shiftDto.getId());
            List<Account> assignedAccount = accounts.stream()
                    .filter(acc -> resultMap.entrySet().stream()
                            .noneMatch(entry -> {
                                String existingShiftId = entry.getKey();
                                List<Account> existingAccounts = entry.getValue();
                                ShiftDto existingShift = shifts.stream()
                                        .filter(s -> s.getId().equals(existingShiftId))
                                        .findFirst()
                                        .orElse(null);

                                return existingAccounts.stream()
                                        .anyMatch(existingAcc -> acc.getId().equals(existingAcc.getId()) &&
                                                isOverlap(shiftDto.getFrom(), shiftDto.getTo(), existingShift.getFrom(), existingShift.getTo()));
                            }))
                    .limit(shiftDto.getCapacity())
                    .map(Converter::account)
                    .toList();
            resultMap.put(shiftDto.getId(), assignedAccount);
        }
        Result result = new Result();
        result.setMap(resultMap);
        Schedule schedule = scheduleRepository.findById(id).get();
        schedule.setResult(result);
        scheduleRepository.save(schedule);
    }

    public boolean calculateResultSalary(String id) {
        List<ShiftDto> shifts = shiftService.getBySchedule(id);
        List<Integer> capacities = new ArrayList<>();
        List<Integer> lengths = new ArrayList<>();
        List<List<AccountDto>> shiftAccounts = new ArrayList<>();
        List<AccountDto> restaurantAccounts = accountService.getByRestaurant();
        for (ShiftDto shiftDto : shifts) {
            List<AccountDto> accounts = shiftService.getRegisteredAccounts(shiftDto.getId());
            shiftAccounts.add(accounts);
            capacities.add(shiftDto.getCapacity());
            Duration duration = Duration.between(shiftDto.getFrom(), shiftDto.getTo());
            lengths.add((int) Math.ceil(duration.toMinutes() / 60.0));
        }
        boolean[][] res = GurobiCalculation.calculate(capacities, lengths, shiftAccounts, restaurantAccounts, shifts);
        if(res == null) {
            calculateResultQueue(id);
            return false;
        } else {
            Result result = new Result();
            Map<String, List<Account>> map = new HashMap<>();
            for(int j = 0; j < shifts.size(); j++) {
                List<Account> assignedAccounts = new LinkedList<>();
                for(int i = 0; i < restaurantAccounts.size(); i++){
                    if(res[i][j]) {
                        assignedAccounts.add(Converter.account(restaurantAccounts.get(i)));
                    }
                }
                map.put(shifts.get(j).getId(), assignedAccounts);
            }
            result.setMap(map);
            Schedule schedule = scheduleRepository.findById(id).get();
            schedule.setResult(result);
            scheduleRepository.save(schedule);
            return true;
        }
    }

    private Schedule adjustScheduleTimeMinus(Schedule schedule) {
        if (schedule != null) {
            schedule.setFrom(schedule.getFrom().minusHours(2));
            schedule.setTo(schedule.getTo().minusHours(2));
        }
        return schedule;
    }

    private Schedule adjustScheduleTimePlus(Schedule schedule) {
        if (schedule != null) {
            schedule.setFrom(schedule.getFrom().plusHours(2));
            schedule.setTo(schedule.getTo().plusHours(2));
        }
        return schedule;
    }

    private boolean isOverlap(LocalDateTime start1, LocalDateTime end1, LocalDateTime start2, LocalDateTime end2) {
        return start1.isBefore(end2) && start2.isBefore(end1);
    }
}
