package cz.cvut.fel.shiftscheduler.dto;

import lombok.Data;

@Data
public class ShiftWithAnswerDto {
    ShiftDto shift;
    Boolean answer;
}
