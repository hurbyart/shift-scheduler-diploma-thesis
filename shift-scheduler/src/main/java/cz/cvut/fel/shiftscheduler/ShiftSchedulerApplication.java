package cz.cvut.fel.shiftscheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class ShiftSchedulerApplication {
	public static void main(String[] args) {
		SpringApplication.run(ShiftSchedulerApplication.class, args);
	}
}
