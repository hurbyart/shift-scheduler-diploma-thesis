package cz.cvut.fel.shiftscheduler.controller;

import cz.cvut.fel.shiftscheduler.dto.ShiftPollDto;
import cz.cvut.fel.shiftscheduler.service.ShiftPollService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@RequestMapping("/shift/poll")
public class ShiftPollController {

    ShiftPollService shiftpollService;

    @GetMapping
    public List<ShiftPollDto> getAll() {
        return shiftpollService.getAll();
    }

    @GetMapping("/{shiftPollId}")
    public ShiftPollDto getById(@PathVariable String shiftPollId) {
        return shiftpollService.getById(shiftPollId);
    }

    @GetMapping("/shift")
    public List<ShiftPollDto> getByShift(@RequestParam String shiftId) {
        return shiftpollService.getByShift(shiftId);
    }

    @PostMapping
    public void create(@RequestBody ShiftPollDto shiftPollDto) {
        shiftpollService.create(shiftPollDto);
    }

    @GetMapping("/user")
    public List<ShiftPollDto> getForCurrentAccount() {
        return shiftpollService.getForCurrentAccount();
    }

    @PutMapping
    public ShiftPollDto update(@RequestBody ShiftPollDto shiftPollDto) {
        return shiftpollService.update(shiftPollDto);
    }

    @DeleteMapping("/{shiftPollId}")
    public void delete(@PathVariable String shiftPollId) {
        shiftpollService.delete(shiftPollId);
    }
}
