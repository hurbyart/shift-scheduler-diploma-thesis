package cz.cvut.fel.shiftscheduler.service;

import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.model.Account;
import cz.cvut.fel.shiftscheduler.repository.AccountRepository;
import cz.cvut.fel.shiftscheduler.repository.RestaurantRepository;
import cz.cvut.fel.shiftscheduler.utils.Converter;
import cz.cvut.fel.shiftscheduler.utils.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class AccountService {

    AccountRepository accountRepository;
    RestaurantRepository restaurantRepository;

    public List<AccountDto> getAll() {
        return accountRepository.findAll().stream().map(Converter::account).toList();
    }

    public AccountDto getById(String id) {
        return Converter.account(accountRepository.findById(id).get());
    }

    public AccountDto getCurrent() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        Account account = accountRepository.findByUsername(new JwtUtil().extractUsername(token)).get();
        return Converter.account(account);
    }

    public List<AccountDto> getByRestaurant() {
        return accountRepository.findAccountsByRestaurantId(getCurrent().getRestaurantDto().getId()).stream()
                .map(Converter::account)
                .toList();
    }

    public void create(AccountDto accountDto) {
        accountRepository.insert(Converter.account(accountDto));
    }

    @Transactional
    public AccountDto update(AccountDto accountDto) {
        accountRepository.delete(accountRepository.findById(accountDto.getId()).get());
        return Converter.account(accountRepository.insert(Converter.account(accountDto)));
    }

    public void delete(String id) {
        accountRepository.delete(accountRepository.findById(id).get());
    }

    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) {
                return accountRepository.findByUsername(username)
                        .orElseThrow(() -> new UsernameNotFoundException("User not found"));
            }
        };
    }
}
