package cz.cvut.fel.shiftscheduler.model;

import lombok.Data;

import javax.persistence.Entity;
import java.util.List;
import java.util.Map;

@Data
@Entity
public class Result {
    Map<String, List<Account>> map;
}
