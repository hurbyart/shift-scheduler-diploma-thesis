package cz.cvut.fel.shiftscheduler.repository;

import cz.cvut.fel.shiftscheduler.model.PollVote;
import cz.cvut.fel.shiftscheduler.model.ShiftPoll;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PollVoteRepository extends MongoRepository<PollVote, String> {
    List<PollVote> findPollVotesByShiftPoll(ShiftPoll shiftPoll);

    List<PollVote> findPollVotesByShiftPoll_Id(String id);

    List<PollVote> findPollVotesByAccount_Id(String id);
}
