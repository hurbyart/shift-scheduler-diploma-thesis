package cz.cvut.fel.shiftscheduler.controller;

import cz.cvut.fel.shiftscheduler.dto.ShiftDto;
import cz.cvut.fel.shiftscheduler.service.ShiftService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class ShiftController {

    ShiftService shiftService;

    @GetMapping("/shift")
    public List<ShiftDto> getAll() {
        return shiftService.getAllCurrent();
    }

    @GetMapping("/schedule/{scheduleId}/shift")
    public List<ShiftDto> getBySchedule(@PathVariable String scheduleId) {
        return shiftService.getBySchedule(scheduleId);
    }

    @PostMapping("/shift")
    public void create(@RequestBody ShiftDto shiftDto) {
        shiftService.create(shiftDto);
    }

    @PutMapping("/shift")
    public ShiftDto update(@RequestBody ShiftDto shiftDto) {
        return shiftService.update(shiftDto);
    }

    @DeleteMapping("/shift/{shiftId}")
    public void delete(@PathVariable String shiftId) {
        shiftService.delete(shiftId);
    }
}
