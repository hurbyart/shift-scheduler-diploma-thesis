package cz.cvut.fel.shiftscheduler.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.util.ProxyUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class Schedule {

    @Id
    @GeneratedValue
    String id;
    String name;
    LocalDateTime from;
    LocalDateTime to;
    @ManyToOne
    Restaurant restaurant;
    Boolean isLocked;
    @OneToOne
    Result result;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ProxyUtils.getUserClass(this) != ProxyUtils.getUserClass(o))
            return false;
        Schedule schedule = (Schedule) o;
        return id != null && Objects.equals(id, schedule.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
