package cz.cvut.fel.shiftscheduler.controller;

import cz.cvut.fel.shiftscheduler.dto.RestaurantDto;
import cz.cvut.fel.shiftscheduler.service.RestaurantService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@RequestMapping("/restaurant")
public class RestaurantController {

    RestaurantService restaurantService;

    @GetMapping
    public List<RestaurantDto> getAll() {
        return restaurantService.getAll();
    }

    @GetMapping("/{restaurantId}")
    public RestaurantDto getById(@PathVariable String restaurantId) {
        return restaurantService.getById(restaurantId);
    }

    @PostMapping
    public void create(@RequestBody RestaurantDto restaurantDto) {
        restaurantService.create(restaurantDto);
    }

    @PutMapping
    public RestaurantDto update(@RequestBody RestaurantDto restaurntDto) {
        return restaurantService.update(restaurntDto);
    }

    @DeleteMapping("/{restaurantId}")
    public void delete(@PathVariable String restaurantId) {
        restaurantService.delete(restaurantId);
    }
}
