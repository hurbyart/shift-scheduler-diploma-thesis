package cz.cvut.fel.shiftscheduler.service;

import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.dto.AuthenticationResponse;
import cz.cvut.fel.shiftscheduler.model.Account;
import cz.cvut.fel.shiftscheduler.model.Restaurant;
import cz.cvut.fel.shiftscheduler.model.Role;
import cz.cvut.fel.shiftscheduler.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final AccountRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    @SneakyThrows
    public AuthenticationResponse signup(AccountDto request) {
        var user = userRepository.findByUsername(request.getUsername());
        if (user.isEmpty()) {
            var newUser = Account.builder().firstName(request.getFirstName()).secondName(request.getSecondName())
                    .username(request.getUsername()).password(passwordEncoder.encode(request.getPassword()))
                    .role(Role.WORKER).position(request.getPosition()).birthDate(request.getBirthDate())
                    .photo(request.getPhoto()).phoneNumber(request.getPhoneNumber())
                    .restaurant(Restaurant.builder()
                            .id("2")
                            .name("Best restaurant")
                            .location("somewhere")
                            .build())
                    .build();
            userRepository.save(newUser);
            var jwt = jwtService.generateToken(newUser);
            return AuthenticationResponse.builder().token(jwt).build();
        } else {
            throw new Exception("ex"); // TODO custom exception
        }
    }

    public AuthenticationResponse signin(AccountDto request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        var user = userRepository.findByUsername(request.getUsername())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
        var jwt = jwtService.generateToken(user);
        return AuthenticationResponse.builder().token(jwt).build();
    }
}
