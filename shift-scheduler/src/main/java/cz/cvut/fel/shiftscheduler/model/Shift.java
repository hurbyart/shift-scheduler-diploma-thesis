package cz.cvut.fel.shiftscheduler.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.util.ProxyUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class Shift {

    @Id
    @GeneratedValue
    String id;
    String name;
    LocalDateTime from;
    LocalDateTime to;
    String note;
    Integer capacity;
    @ManyToOne
    Schedule schedule;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ProxyUtils.getUserClass(this) != ProxyUtils.getUserClass(o))
            return false;
        Shift shift = (Shift) o;
        return id != null && Objects.equals(id, shift.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
