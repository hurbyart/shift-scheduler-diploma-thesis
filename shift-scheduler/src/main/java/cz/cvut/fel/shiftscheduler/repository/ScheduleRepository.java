package cz.cvut.fel.shiftscheduler.repository;

import cz.cvut.fel.shiftscheduler.model.Schedule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends MongoRepository<Schedule, String> {
    List<Schedule> findSchedulesByRestaurant_Id(String id);
}
