package cz.cvut.fel.shiftscheduler.repository;

import cz.cvut.fel.shiftscheduler.model.Shift;
import cz.cvut.fel.shiftscheduler.model.ShiftPoll;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ShiftPollRepository extends MongoRepository<ShiftPoll, String> {
    Optional<ShiftPoll> findShiftPollByShift(Shift shift);

    List<ShiftPoll> findShiftPollsByShift_Id(String id);

    List<ShiftPoll> findShiftPollsByAccount_Id(String id);
}
