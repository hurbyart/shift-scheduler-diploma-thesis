package cz.cvut.fel.shiftscheduler.dto;

import cz.cvut.fel.shiftscheduler.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
public class AccountDto {
    String id;
    String username;
    String password;
    String firstName;
    String secondName;
    String position;
    LocalDate birthDate;
    String photo;
    String phoneNumber;
    Double salary;
    private Role role;
    RestaurantDto restaurantDto;
    List<ShiftPollDto> shiftPollDtoList;
}
