package cz.cvut.fel.shiftscheduler.repository;

import cz.cvut.fel.shiftscheduler.model.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends MongoRepository<Account, String> {
    Optional<Account> findByUsername(String username);

    List<Account> findAccountsByRestaurantId(String id);
}
