package cz.cvut.fel.shiftscheduler.service;

import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.dto.ShiftDto;
import cz.cvut.fel.shiftscheduler.dto.ShiftPollDto;
import cz.cvut.fel.shiftscheduler.model.ShiftPoll;
import cz.cvut.fel.shiftscheduler.repository.ShiftPollRepository;
import cz.cvut.fel.shiftscheduler.repository.ShiftRepository;
import cz.cvut.fel.shiftscheduler.utils.Converter;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ShiftPollService {

    ShiftPollRepository shiftPollRepository;
    ShiftRepository shiftRepository;
    ShiftService shiftService;
    AccountService accountService;
    PollVoteService pollVoteService;

    public List<ShiftPollDto> getAll() {
        return shiftPollRepository.findAll().stream().map(Converter::shiftPoll).toList();
    }

    public List<ShiftPollDto> getForCurrentAccount() {
        List<ShiftPollDto> l = shiftPollRepository.findShiftPollsByAccount_Id(accountService.getCurrent().getId()).stream()
                .map(Converter::shiftPoll)
                .peek(shiftPoll -> {
                    ShiftDto shift = adjustShiftTime(shiftPoll.getShift());
                    shift.setRegistered(shiftService.getRegisteredAccounts(shiftPoll.getShift().getId()));
                    shiftPoll.setShift(shift);
                })
                .toList();
        return l;
    }

    public ShiftPollDto getById(String id) {
        return Converter.shiftPoll(shiftPollRepository.findById(id).get());
    }

    public List<ShiftPollDto> getByShift(String shiftId) {
        List<ShiftPoll> shiftPolls =
                shiftPollRepository.findShiftPollsByShift_Id(shiftRepository.findById(shiftId).get().getId());
        return shiftPolls.stream().map(Converter::shiftPoll).toList();
    }

    public void create(ShiftPollDto shiftPollDto) {
        for (AccountDto account : accountService.getByRestaurant()) {
            shiftPollDto.setAccount(account);
            ShiftPoll shiftPoll = Converter.shiftPoll(shiftPollDto);
            shiftPollRepository.insert(shiftPoll);
        }
    }

    public ShiftPollDto update(ShiftPollDto shiftPollDto) {
        shiftPollRepository.delete(shiftPollRepository.findById(shiftPollDto.getId()).get());
        return Converter.shiftPoll(shiftPollRepository.insert(Converter.shiftPoll(shiftPollDto)));
    }

    public void delete(String id) {
        pollVoteService.getByShiftPoll(id).stream()
                .forEach(pollVoteDto -> {
                    pollVoteService.delete(pollVoteDto.getId());
                });
        shiftPollRepository.delete(shiftPollRepository.findById(id).get());
    }

    private ShiftDto adjustShiftTime(ShiftDto shiftDto) {
        if (shiftDto != null) {
            shiftDto.setFrom(shiftDto.getFrom().minusHours(2));
            shiftDto.setTo(shiftDto.getTo().minusHours(2));
        }
        return shiftDto;
    }
}
