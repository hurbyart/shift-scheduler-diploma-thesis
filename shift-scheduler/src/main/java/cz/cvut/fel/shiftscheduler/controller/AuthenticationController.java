package cz.cvut.fel.shiftscheduler.controller;

import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.dto.AuthenticationResponse;
import cz.cvut.fel.shiftscheduler.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping("/signup")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<AuthenticationResponse> signup(@RequestBody AccountDto request) {
        return ResponseEntity.ok(authenticationService.signup(request));
    }

    @PostMapping("/signin")
    public ResponseEntity<AuthenticationResponse> signin(@RequestBody AccountDto request) {
        return ResponseEntity.ok(authenticationService.signin(request));
    }
}
