package cz.cvut.fel.shiftscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
public class ShiftDto {
    String id;
    String name;
    LocalDateTime from;
    LocalDateTime to;
    List<AccountDto> registered;
    Integer capacity;
    String note;
    String scheduleId;
}
