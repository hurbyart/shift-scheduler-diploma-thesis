package cz.cvut.fel.shiftscheduler.service;

import cz.cvut.fel.shiftscheduler.dto.RestaurantDto;
import cz.cvut.fel.shiftscheduler.repository.RestaurantRepository;
import cz.cvut.fel.shiftscheduler.utils.Converter;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class RestaurantService {

    RestaurantRepository restaurantRepository;

    public List<RestaurantDto> getAll() {
        return restaurantRepository.findAll().stream().map(Converter::restaurant).toList();
    }

    public RestaurantDto getById(String id) {
        return Converter.restaurant(restaurantRepository.findById(id).get());
    }

    public void create(RestaurantDto restaurantDto) {
        restaurantRepository.insert(List.of(Converter.restaurant(restaurantDto)));
    }

    @Transactional
    public RestaurantDto update(RestaurantDto restaurantDto) {
        restaurantRepository.delete(restaurantRepository.findById(restaurantDto.getId()).get());
        return Converter.restaurant(restaurantRepository.insert(Converter.restaurant(restaurantDto)));
    }

    public void delete(String id) {
        restaurantRepository.delete(restaurantRepository.findById(id).get());
    }
}
