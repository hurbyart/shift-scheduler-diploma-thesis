package cz.cvut.fel.shiftscheduler.service;

import cz.cvut.fel.shiftscheduler.dto.AccountDto;
import cz.cvut.fel.shiftscheduler.dto.ShiftDto;
import cz.cvut.fel.shiftscheduler.model.PollVote;
import cz.cvut.fel.shiftscheduler.model.Shift;
import cz.cvut.fel.shiftscheduler.model.ShiftPoll;
import cz.cvut.fel.shiftscheduler.repository.PollVoteRepository;
import cz.cvut.fel.shiftscheduler.repository.ScheduleRepository;
import cz.cvut.fel.shiftscheduler.repository.ShiftPollRepository;
import cz.cvut.fel.shiftscheduler.repository.ShiftRepository;
import cz.cvut.fel.shiftscheduler.utils.Converter;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ShiftService {

    ShiftRepository shiftRepository;
    ScheduleRepository scheduleRepository;
    AccountService accountService;
    private final PollVoteRepository pollVoteRepository;
    private final ShiftPollRepository shiftPollRepository;

    public List<ShiftDto> getAll() {
        return shiftRepository.findAll().stream()
                .map(this::adjustShiftTimeMinus)
                .map(Converter::shift)
                .toList();
    }

    public ShiftDto getById(String id) {
        return Converter.shift(adjustShiftTimeMinus(shiftRepository.findById(id).get()));
    }

    public List<ShiftDto> getBySchedule(String scheduleId) {
        return shiftRepository.findShiftsBySchedule_Id(scheduleId).stream()
                .map(this::adjustShiftTimeMinus)
                .map(Converter::shift)
                .peek(shift -> shift.setRegistered(getRegisteredAccounts(shift.getId())))
                .toList();
    }

    public List<AccountDto> getRegisteredAccounts(String shiftId) {
        List<AccountDto> result = new ArrayList<>();
        List<ShiftPoll> shiftPolls = shiftPollRepository.findShiftPollsByShift_Id(shiftId);
        if (!shiftPolls.isEmpty()) {
            for (ShiftPoll shiftPoll : shiftPolls) {
                List<PollVote> pollVotes = pollVoteRepository.findPollVotesByShiftPoll_Id(shiftPoll.getId());
                if (!pollVotes.isEmpty() && pollVotes.get(pollVotes.size() - 1).getAnswer()) {
                    result.add(Converter.account(shiftPoll.getAccount()));
                }
            }
        }
        return result;
    }

    public List<ShiftDto> getAllCurrent() {
        return pollVoteRepository.findPollVotesByAccount_Id(accountService.getCurrent().getId()).stream()
                .map(PollVote::getShiftPoll)
                .map(ShiftPoll::getShift)
                .distinct()
                .map(this::adjustShiftTimeMinus)
                .map(Converter::shift)
                .toList();
    }

    public void create(ShiftDto shiftDto) {
        Shift shift = adjustShiftTimePlus(Converter.shift(shiftDto));
        shift.setSchedule(scheduleRepository.findById(shiftDto.getScheduleId()).get());
        shiftRepository.insert(shift);
    }

    @Transactional
    public ShiftDto update(ShiftDto shiftDto) {
        Shift shift = adjustShiftTimePlus(Converter.shift(shiftDto));
        shift.setSchedule(scheduleRepository.findById(shiftDto.getScheduleId()).get());
        return Converter.shift(shiftRepository.save(shift));
    }

    public void delete(String id) {
        shiftPollRepository.deleteAll(shiftPollRepository.findShiftPollsByShift_Id(id));
        shiftRepository.delete(shiftRepository.findById(id).get());
    }

    private Shift adjustShiftTimeMinus(Shift shift) {
        if (shift != null) {
            shift.setFrom(shift.getFrom().minusHours(2));
            shift.setTo(shift.getTo().minusHours(2));
        }
        return shift;
    }

    private Shift adjustShiftTimePlus(Shift shift) {
        if (shift != null) {
            shift.setFrom(shift.getFrom().plusHours(2));
            shift.setTo(shift.getTo().plusHours(2));
        }
        return shift;
    }
}
