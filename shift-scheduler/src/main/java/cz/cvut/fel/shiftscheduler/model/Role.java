package cz.cvut.fel.shiftscheduler.model;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
public enum Role implements GrantedAuthority {
    MANAGER("MANAGER"), WORKER("WORKER");

    private final String name;

    @Override
    public String getAuthority() {
        return name();
    }

}
