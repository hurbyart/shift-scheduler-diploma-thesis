package cz.cvut.fel.shiftscheduler.utils;

import cz.cvut.fel.shiftscheduler.dto.*;
import cz.cvut.fel.shiftscheduler.model.*;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Converter {

    public RestaurantDto restaurant(Restaurant restaurant) {
        return RestaurantDto.builder()
                .id(restaurant.getId())
                .name(restaurant.getName())
                .location(restaurant.getLocation())
                .build();
    }

    public Restaurant restaurant(RestaurantDto restaurant) {
        return Restaurant.builder()
                .id(restaurant.getId())
                .name(restaurant.getName())
                .location(restaurant.getLocation())
                .build();
    }

    public AccountDto account(Account account) {
        return AccountDto.builder()
                .id(account.getId())
                .username(account.getUsername())
                .password(account.getPassword())
                .firstName(account.getFirstName())
                .secondName(account.getSecondName())
                .position(account.getPosition())
                .birthDate(account.getBirthDate())
                .photo(account.getPhoto())
                .phoneNumber(account.getPhoneNumber())
                .salary(account.getSalary())
                .role(account.getRole())
                .restaurantDto(account.getRestaurant() == null ? null : Converter.restaurant(account.getRestaurant()))
                .build();
    }

    public Account account(AccountDto accountDto) {
        return Account.builder()
                .id(accountDto.getId())
                .username(accountDto.getUsername())
                .password(accountDto.getPassword())
                .firstName(accountDto.getFirstName())
                .secondName(accountDto.getSecondName())
                .position(accountDto.getPosition())
                .birthDate(accountDto.getBirthDate())
                .photo(accountDto.getPhoto())
                .phoneNumber(accountDto.getPhoneNumber())
                .salary(accountDto.getSalary())
                .role(accountDto.getRole())
                //.restaurant(accountDto.getRestaurantDto() == null ? null : Converter.restaurant(accountDto.getRestaurantDto()))
                .restaurant(Restaurant.builder()
                        .id("2")
                        .name("Best restaurant")
                        .location("somewhere")
                        .build())// TODO
                .build();
    }

    public PollVote pollVote(PollVoteDto pollVoteDto) {
        return PollVote.builder()
                .id(pollVoteDto.getId())
                .answer(pollVoteDto.getAnswer())
                .shiftPoll(shiftPoll(pollVoteDto.getShiftPollDto()))
                .account(account(pollVoteDto.getAccountDto()))
                .build();
    }

    public PollVoteDto pollVote(PollVote pollVote) {
        return PollVoteDto.builder()
                .id(pollVote.getId())
                .answer(pollVote.getAnswer())
                .shiftPollDto(shiftPoll(pollVote.getShiftPoll()))
                .accountDto(account(pollVote.getAccount()))
                .build();
    }

    public ShiftPoll shiftPoll(ShiftPollDto shiftPollDto) {
        return ShiftPoll.builder()
                .id(shiftPollDto.getId())
                .shift(shift(shiftPollDto.getShift()))
                .account(account(shiftPollDto.getAccount()))
                .build();
    }

    public ShiftPollDto shiftPoll(ShiftPoll shiftPoll) {
        return ShiftPollDto.builder()
                .id(shiftPoll.getId())
                .shift(shift(shiftPoll.getShift()))
                .account(shiftPoll.getAccount() == null ? null : account(shiftPoll.getAccount()))
                .build();
    }

    public Shift shift(ShiftDto shiftDto) {
        return Shift.builder()
                .id(shiftDto.getId())
                .name(shiftDto.getName())
                .from(shiftDto.getFrom())
                .to(shiftDto.getTo())
                .note(shiftDto.getNote())
                .capacity(shiftDto.getCapacity())
                .build();
    }

    public ShiftDto shift(Shift shift) {
        return ShiftDto.builder()
                .id(shift.getId())
                .name(shift.getName())
                .from(shift.getFrom())
                .to(shift.getTo())
                .note(shift.getNote())
                .capacity(shift.getCapacity())
                .scheduleId(shift.getSchedule() == null ? null : shift.getSchedule().getId())
                .build();
    }

    public Schedule schedule(ScheduleDto scheduleDto) {
        return Schedule.builder()
                .id(scheduleDto.getId())
                .name(scheduleDto.getName())
                .from(scheduleDto.getFrom())
                .to(scheduleDto.getTo())
                .restaurant(restaurant(scheduleDto.getRestaurant()))
                .isLocked(scheduleDto.getIsLocked())
                .build();
    }

    public ScheduleDto schedule(Schedule schedule) {
        return ScheduleDto.builder()
                .id(schedule.getId())
                .name(schedule.getName())
                .from(schedule.getFrom())
                .to(schedule.getTo())
                .restaurant(restaurant(schedule.getRestaurant()))
                .isLocked(schedule.getIsLocked())
                .build();
    }
}
