import { Component, OnInit } from '@angular/core';
import { Employee } from "../constants/employee";
import { UserService } from "../_services/account.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  employee: Employee | undefined;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getCurrent().subscribe({
      next: (data: Employee) => {
        this.employee = data;
      },
      error: (error) => {
        console.error('Failed to load employee data', error);
      }
    });
  }
}
