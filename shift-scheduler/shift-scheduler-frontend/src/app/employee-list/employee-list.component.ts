import { Component } from '@angular/core';
import {Employee} from "../constants/employee";
import {MatExpansionModule} from "@angular/material/expansion";
import {DatePipe} from "@angular/common";
import {ScheduleService} from "../_services/schedule.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Schedule} from "../constants/schedule";
import {UserService} from "../_services/account.service";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
})
export class EmployeeListComponent {
  employees: Employee[] = [];

  constructor(private accountService: UserService) {


    accountService.getByRestaurant().subscribe({
      next: data => {
        this.employees = data as Employee[];
      }});
  }
}
