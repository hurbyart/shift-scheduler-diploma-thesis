import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {DemoComponent} from "./calendar/calendar.component";
import {ShiftComponent} from "./shift/shift.component";
import {ScheduleComponent} from "./schedule/schedule.component";
import {EmployeeListComponent} from "./employee-list/employee-list.component";
import {ShiftPollComponent} from "./shift-poll/shift-poll.component";
import {ProfileComponent} from "./profile/profile.component";

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'calendar', component: DemoComponent},
  {path: 'shift', component: ShiftComponent},
  {path: 'schedule', component: ScheduleComponent},
  {path: 'employee', component: EmployeeListComponent},
  {path: 'shift-poll', component: ShiftPollComponent},
  {path: 'profile', component: ProfileComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
