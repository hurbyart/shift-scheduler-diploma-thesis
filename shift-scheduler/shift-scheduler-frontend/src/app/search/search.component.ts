import {Component, ViewChild} from '@angular/core';
import {StorageService} from "../_services/storage.service";
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../_services/user.service";
import {CocktailsResponse} from "../constants/cocktailsResponse";
import {MatAccordion} from '@angular/material/expansion';
import {CaloriesResponse} from "../constants/caloriesResponse";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  @ViewChild(MatAccordion) accordion: MatAccordion | undefined;
  @ViewChild('matExpansionPanel') _matExpansionPanel: any;
  form: FormGroup = new FormGroup({
    search: new FormControl(''),
  });
  submitted = false;
  errorMessage = '';
  cocktails: CocktailsResponse[] = [];
  info: CaloriesResponse[] = [];
  ingredientName: string[] = [];
  iterator: number = 0;
  emptyCocktails = false;

  constructor(private userService: UserService,
              private storageService: StorageService,
              private formBuilder: FormBuilder,) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      search: [
        '',
        [
          Validators.required,
        ]
      ]
    });
  }

  onSubmit(): void {
    const {search} = this.form.value;
    this.emptyCocktails = false;
    this.submitted = true;

    this.userService.getCocktails(search).subscribe({
      next: data => {
        this.cocktails = data as CocktailsResponse[];
        if (this.cocktails.length === 0) this.emptyCocktails = true;
      }
    });
  }

  reloadPage(): void {
    window.location.reload();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  showCalories(ingredient: string): void {
    this.userService.getCalories(ingredient).subscribe({
      next: data => {
        if (data.items.length !== 0) {
          this.info[this.iterator] = data;
          this.ingredientName[this.iterator] = ingredient;
          this.iterator++;
        }
      }
    });

  }

  findNumber(ingredient: string): number {
    return this.ingredientName.findIndex(item => item == ingredient);
  }

  addToFavorites(ingredientName: string) {
    this.userService.addFavorite(this.storageService.getUser().id, ingredientName).subscribe();
  }


}
