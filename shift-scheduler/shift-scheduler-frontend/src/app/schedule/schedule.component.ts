import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Schedule} from "../constants/schedule";
import {ScheduleService} from "../_services/schedule.service";
import {Router} from "@angular/router";
import {UserService} from "../_services/account.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent {
  shiftForm: FormGroup;
  schedules: Schedule[] = [];
  role: String;

  constructor(private scheduleService: ScheduleService, private fb: FormBuilder, private router: Router,
              private accountService: UserService, private snackBar: MatSnackBar) {
    this.shiftForm = this.fb.group({
      name: ['', Validators.required],
      fromDate: ['', Validators.required],
      fromTime: ['', Validators.required],
      toDate: ['', Validators.required],
      toTime: ['', Validators.required],
    });

    this.role = "";
    accountService.getCurrent().subscribe({
      next: data => {
        this.role = data.role;
      }});

    this.getSchedules();
  }

  getSchedules(): any {
    this.scheduleService.getAll().subscribe({
      next: data => {
        this.schedules = this.processScheduleData(data).map(schedule => ({ ...schedule, isEditing: false }));
      }});
  }

  submitForm(): void {
    if (this.shiftForm.valid) {
      const formValue = this.shiftForm.value;

      const fromDateTime = this.combineDateAndTime(formValue.fromDate, formValue.fromTime);
      const toDateTime = this.combineDateAndTime(formValue.toDate, formValue.toTime)


      this.scheduleService.create(formValue.name, fromDateTime, toDateTime).subscribe({
        next: () => {
          this.shiftForm.reset();
          this.getSchedules();
        },
        error: (error) => {
          console.error('Error creating shift:', error);
        }
      });
      this.schedules.push() // TODO
    } else {
      console.error('Form is not valid:', this.shiftForm.errors);
    }
  }

  watchShifts(scheduleId: string): void {
    window.sessionStorage.setItem("scheduleId", scheduleId);
    this.router.navigate([`/shift`]);

  }

  deleteSchedule(scheduleId: string): void {
    console.log(scheduleId);
    this.scheduleService.delete(scheduleId).subscribe({
      next: () => {
        this.getSchedules();
      },
      error: (error) => {
        console.error('Error creating shift:', error);
      }
    });
  }

  combineDateAndTime(date: any, time: string): string {
    if (!(date instanceof Date)) {
      date = new Date(date);
    }

    const timeParts = time.split(':');
    date.setHours(parseInt(timeParts[0], 10));
    date.setMinutes(parseInt(timeParts[1], 10));

    return new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString();
  }


  processScheduleData(schedules: any[]): Schedule[] {
    return schedules.map(schedule => {
      const from = new Date(schedule.from);
      const to = new Date(schedule.to);
      console.log(from);
      console.log(to);

      return {
        ...schedule,
        fromDate: from.toISOString().split('T')[0],
        fromTime: from.toTimeString().split(' ')[0].substring(0, 5),
        toDate: to.toISOString().split('T')[0],
        toTime: to.toTimeString().split(' ')[0].substring(0, 5),
      };
    });
  }

  updateSchedule(schedule: Schedule): void {
    console.log(schedule);
    this.scheduleService.update(schedule.id, schedule.name, this.combineDateAndTime(schedule.fromDate, schedule.fromTime),
      this.combineDateAndTime(schedule.toDate, schedule.toTime)).subscribe({
      next: () => {
      },
      error: (error) => {
        console.error('Error creating shift:', error);
      }
    });
  }

  calculateSchedule(scheduleId: string, status: boolean) {
    this.scheduleService.calculateSchedule(scheduleId, status).subscribe({
      next: (noSuccess) => {
        console.log(noSuccess)
        if (noSuccess) {
          this.snackBar.open('It was not possible to create the cheapest schedule. It is possible that you ' +
            'don\'t have enough workers accepted some shift from this schedule.' +
            'Schedule by queue was created instead.', 'Close', {
            duration: 9000,
            horizontalPosition: 'right',
            verticalPosition: 'bottom',
          });
        } else {
        this.snackBar.open('Schedule was successfully created!', 'Close', {
            duration: 9000,
            horizontalPosition: 'right',
            verticalPosition: 'bottom',
          });
        }
        this.getSchedules();
      },
      error: (error) => {
        console.error('Error occurred:', error);
        this.snackBar.open('Error occurred while processing the schedule.', 'Close', {
          duration: 3000,
          horizontalPosition: 'right',
          verticalPosition: 'bottom',
        });
      }
    });
  }
}
