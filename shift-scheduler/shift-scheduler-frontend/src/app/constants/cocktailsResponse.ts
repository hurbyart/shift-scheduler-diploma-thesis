export interface CocktailsResponse {
  ingredients: string[],
  instructions: string,
  name: string
}
