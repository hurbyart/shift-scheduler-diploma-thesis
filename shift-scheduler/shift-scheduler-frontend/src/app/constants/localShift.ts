export interface LocalShift {
  id: string;
  from: string;
  to: string;
  name: string;
  note: string;
  capacity: string;
  scheduleId: string;
  isEditing: boolean;
}
