export const ApiUrl = {
  baseUrl: 'http://127.0.0.1:8080/shiftScheduler/api/v1',
  shift: '/shift',
  user: '/auth/signup',
  username: '/getUsername',
  login: '/auth/signin',
  favorites: '/favorites',
  cocktailsByName: '/cocktails/get?searchedName=',
  caloriesByName: '/calories/get?name=',
  schedule: '/schedule',
  account: '/account',
  restaurant: '/restaurant',
  poll: '/poll',
  vote: '/vote'
};
