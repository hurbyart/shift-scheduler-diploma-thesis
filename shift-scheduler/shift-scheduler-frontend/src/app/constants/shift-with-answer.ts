import {Shift} from "./shift";

export interface ShiftWithAnswer {
  shift: Shift,
  answer: boolean | null
}
