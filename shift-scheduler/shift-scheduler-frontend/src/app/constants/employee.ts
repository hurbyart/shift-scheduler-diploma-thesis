export interface Employee {
  id: string;
  username: string;
  password: string;
  firstName: string;
  secondName: string;
  position: string;
  birthDate: string;
  photo: string;
  salary: string;
  phoneNumber: string;
  role: string;
}
