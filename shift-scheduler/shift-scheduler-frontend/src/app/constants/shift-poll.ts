import {Shift} from "./shift";
import {Employee} from "./employee";

export interface ShiftPoll {
  id: string;
  shift: Shift;
  account: Employee;
}
