import {CaloriesObject} from "./caloriesObject";

export interface CaloriesResponse {
  items: CaloriesObject[]
}
