import {Shift} from "./shift";
import {ShiftPoll} from "./shift-poll";
import {Employee} from "./employee";

export interface PollVote {
  id: string;
  shiftPoll: ShiftPoll,
  account: Employee,
  answer: Boolean
}
