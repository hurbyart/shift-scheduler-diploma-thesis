import {Employee} from "./employee";

export interface Shift {
  id: string;
  name: string;
  fromDate: string; // LocalDateTime as ISO string
  fromTime: string; // LocalDateTime as ISO string
  toDate: string;   // LocalDateTime as ISO string
  toTime: string;   // LocalDateTime as ISO string
  note: string;
  capacity: string;
  registered: Employee[];
  scheduleId: string;
  isEditing: boolean;
}
