import {Component, OnInit} from '@angular/core';
import {AuthService} from '../_services/auth.service';
import {StorageService} from '../_services/storage.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  submitted = false;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService,
              private storageService: StorageService,
              private formBuilder: FormBuilder,
              private router: Router,) {
  }

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
    }
    this.form = this.formBuilder.group({
      username: [
        '',
        [
          Validators.required,
        ]
      ],
      password: [
        '',
        [
          Validators.required,
        ]
      ],
    });
  }

  onSubmit(): void {
    const {username, password} = this.form.value;
    this.submitted = true;

    this.authService.login(username, password).subscribe({
      next: data => {
        this.storageService.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.router.navigate(["/calendar"]).then(() => this.reloadPage());
      },
      error: err => {
        this.errorMessage = err.error.text;
        this.isLoginFailed = true;
      }
    });
  }

  reloadPage(): void {
    window.location.reload();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }
}
