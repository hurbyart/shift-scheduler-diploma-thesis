import {Component, OnInit} from '@angular/core';
import {UserService} from '../_services/user.service';
import {StorageService} from "../_services/storage.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  content?: string;

  currentUser: any;
  username: string | undefined;
  isLoggedIn = false;

  constructor(private userService: UserService, private storageService: StorageService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();
    this.currentUser = this.storageService.getUser();
  }
}
