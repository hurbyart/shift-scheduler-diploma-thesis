import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import {catchError, forkJoin, map, of, Subject, switchMap} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import {EventColor} from 'calendar-utils';
import {Shift} from "../constants/shift";
import {ShiftService} from "../_services/shift.service";
import {ScheduleService} from "../_services/schedule.service";
import {Schedule} from "../constants/schedule";
import {PollVoteService} from "../_services/poll-vote.service";
import {ShiftWithAnswer} from "../constants/shift-with-answer";

const colors: Record<string, EventColor> = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
  green: {
    primary: '#28a745', // Fully opaque green
    secondary: '#C3E6CB', // Lighter green, you can use an online tool to choose a lighter green hex
  },
  greentransparent: {
    primary: 'rgba(40, 167, 69, 0.3)', // Semi-transparent green, 50% opacity
    secondary: 'rgba(40, 167, 69, 0.1)', // Lighter and more transparent green, 30% opacity
  },
};

@Component({
  selector: 'mwl-demo-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      h3 {
        margin: 0 0 10px;
      }

      pre {
        background-color: #f5f5f5;
        padding: 15px;
      }
    `,
  ],
  templateUrl: 'calendar.component.html',
})
export class DemoComponent {
  @ViewChild('modalContent', {static: true}) modalContent: TemplateRef<any> | undefined;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  } | undefined;

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({event}: { event: CalendarEvent }): void => {
        // this.handleEvent('Edited', event);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({event}: { event: CalendarEvent }): void => {
        //this.events = this.events.filter((iEvent) => iEvent !== event);
        //  this.handleEvent('Deleted', event);
      },
    },
  ];

  refresh = new Subject<void>();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  constructor(private modal: NgbModal, private shiftService: ShiftService, private scheduleService: ScheduleService,
              private pollVoteService: PollVoteService) {
  }

  ngOnInit() {
    this.loadShifts(); // Load shifts when the component initializes
    this.loadSchedules();
  }

  dayClicked({date, events}: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
                      event,
                      newStart,
                      newEnd,
                    }: CalendarEventTimesChangedEvent): void {
    // this.events = this.events.map((iEvent) => {
    //   if (iEvent === event) {
    //     return {
    //       ...event,
    //       start: newStart,
    //       end: newEnd,
    //     };
    //   }
    //   return iEvent;
    // });
    // this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = {event, action};
    this.modal.open(this.modalContent, {size: 'lg'});
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors['red'],
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
      },
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }


  private convertScheduleToCalendarEvent(schedule: Schedule): CalendarEvent {
    return {
      start: new Date(schedule.fromDate + 'T' + schedule.fromTime),
      end: new Date(schedule.toDate + 'T' + schedule.toTime),
      title: schedule.name,
      color: colors['blue'], // Example: set color based on some property or logic
      meta: {
        scheduleId: schedule.id,
        note: schedule.note,
      },
      allDay: true,
      draggable: false,
      resizable: {
        beforeStart: false,
        afterEnd: false
      }
    };
  }

  private loadShifts(): void {
    this.pollVoteService.getByShiftWithAnswer().subscribe(shiftsWithAnswers => {
      // First, process the shift data to get the fromDate, fromTime, toDate, toTime properties
      const processedShifts = this.processShiftData(shiftsWithAnswers.map((swa: any[]) => swa.shift));

      console.log(processedShifts);
      const shiftEvents: CalendarEvent[] = processedShifts.map((shift, index) => {
        // Use the index to find the corresponding answer from the original shiftsWithAnswers array
        const answer = shiftsWithAnswers[index].answer;
        const color = this.getColorBasedOnAnswer(answer);
        return this.convertShiftToCalendarEvent(shift, color);
      });

      console.log(shiftEvents);
      this.events = [...this.events, ...shiftEvents];
      this.refresh.next(); // Refresh the view to reflect new data
    }, error => {
      console.error('Error loading shifts with answers:', error);
    });
  }

// ... (rest of the code remains unchanged)


  private getColorBasedOnAnswer(answer: boolean | null): EventColor {
    // Return the color object based on the answer
    if (answer === null) {
      return colors['yellow']; // No answer is equivalent to null
    }
    return answer ? colors['green'] : colors['greentransparent']; // True is green, false is greentransparent
  }

  private convertShiftToCalendarEvent(shift: Shift, color: EventColor): CalendarEvent {
    return {
      start: new Date(shift.fromDate + 'T' + shift.fromTime),
      end: new Date(shift.toDate + 'T' + shift.toTime),
      title: shift.name,
      color: color,
      meta: {
        shiftId: shift.id,
        note: shift.note,
        registered: shift.registered,
        capacity: shift.capacity
      },
      draggable: false,
      resizable: {
        beforeStart: false,
        afterEnd: false
      }
    };
  }


  private loadSchedules(): void {
    this.scheduleService.getAll().subscribe(schedules => {
      const scheduleEvents = this.processScheduleData(schedules).map((schedule: Schedule) => this.convertScheduleToCalendarEvent(schedule));
      this.events = [...this.events, ...scheduleEvents];
      this.refresh.next(); // Refresh the view to reflect new data
    });
  }


  processShiftData(shifts: any[]): Shift[] {
    return shifts.map(shift => {
      const from = new Date(shift.from);
      const to = new Date(shift.to);

      console.log(shift.registered);

      return {
        ...shift,
        fromDate: from.toISOString().split('T')[0], // extracts the date part
        fromTime: from.toTimeString().split(' ')[0].substring(0, 5), // extracts the time part
        toDate: to.toISOString().split('T')[0],
        toTime: to.toTimeString().split(' ')[0].substring(0, 5),
      };
    });
  }

  processScheduleData(schedules: any[]): Schedule[] {
    return schedules.map(schedule => {
      const from = new Date(schedule.from);
      const to = new Date(schedule.to);

      console.log(schedule.note); // Assuming you might want to log this for some reason

      return {
        ...schedule,
        fromDate: from.toISOString().split('T')[0], // extracts the date part
        fromTime: from.toTimeString().split(' ')[0].substring(0, 5), // extracts the time part
        toDate: to.toISOString().split('T')[0],
        toTime: to.toTimeString().split(' ')[0].substring(0, 5),
      };
    });
  }
}
