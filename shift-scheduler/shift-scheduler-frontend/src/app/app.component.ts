import {Component} from '@angular/core';
import {StorageService} from './_services/storage.service';
import {UserService} from "./_services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string | undefined;

  constructor(private storageService: StorageService, private userService: UserService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.userService.getUsername(user.id).subscribe(data => {
        this.username = data;
      });
    }
  }

  logout(): void {
    this.storageService.clean();
  }
}
