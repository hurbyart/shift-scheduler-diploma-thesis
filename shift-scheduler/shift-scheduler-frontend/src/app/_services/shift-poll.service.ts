import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiUrl} from "../constants/api.url";
import {Shift} from "../constants/shift";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root',
})
export class ShiftPollService {
  CurrentUser: any;

  constructor(private http: HttpClient) {
  }

  send(shift: Shift | null): Observable<any> {
    console.log(ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll);
    return this.http.post(
      ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll,
      {shift},
      httpOptions)
  }

  getByShift(shiftId: String): Observable<any> {
    console.log(ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll);
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll + ApiUrl.shift + '?shiftId=' + shiftId)
  }

  getForCurrentAccount(): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll + "/user"
    )
  }
}
