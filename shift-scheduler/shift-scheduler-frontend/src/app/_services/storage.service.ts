import {Injectable} from '@angular/core';

const USER_KEY = 'auth-user';

interface UserEntity {
  jwt: string,
  id: number
}

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() {
  }

  clean(): void {
    window.sessionStorage.clear();
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);
    console.log(user)
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }

  public getToken() {
    const user = JSON.parse(window.sessionStorage.getItem(USER_KEY)!) as UserEntity;
    console.log("user" + user.jwt);
    if (user) {
      return user.jwt;
    }
    return null;
  }

  public isLoggedIn(): boolean {
    const user = window.sessionStorage.getItem(USER_KEY);
    return !!user;
  }
}
