import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiUrl} from "../constants/api.url";

@Injectable({
  providedIn: 'root',
})
export class UserService {
  CurrentUser: any;

  constructor(private http: HttpClient) {
  }

  getAllFavorites(id: string): Observable<any> {
    return this.http.get(ApiUrl.baseUrl + '/' + id + ApiUrl.favorites, {responseType: 'json'});
  }

  addFavorite(id: string, name: string): Observable<any> {
    return this.http.post(ApiUrl.baseUrl + "/" + id + ApiUrl.favorites + '?name=' + name, {responseType: 'json'});
  }

  getUsername(id: string): Observable<any> {
    return this.http.get(ApiUrl.baseUrl + ApiUrl.username + '/' + id, {responseType: 'text'});
  }

  getCocktails(text: string): Observable<any> {
    return this.http.get(ApiUrl.baseUrl + ApiUrl.cocktailsByName + text, {responseType: 'json'});
  }

  getCalories(text: string): Observable<any> {
    return this.http.get(ApiUrl.baseUrl + ApiUrl.caloriesByName + text, {responseType: 'json'});
  }
}
