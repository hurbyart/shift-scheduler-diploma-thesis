import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ApiUrl} from "../constants/api.url";
import {ShiftPoll} from "../constants/shift-poll";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root',
})
export class PollVoteService {
  CurrentUser: any;

  constructor(private http: HttpClient) {
  }

  create(answer: Boolean, shiftPollDto: ShiftPoll): Observable<any> {
    console.log(shiftPollDto)
    return this.http.post(
      ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll + ApiUrl.vote,
      {answer, shiftPollDto},
      httpOptions)
  }

  getByShiftPollId(shiftPollId: String): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll + ApiUrl.vote + '/shiftPollId' + '?shiftPollId=' + shiftPollId,
    )
  }

  getByShiftWithAnswer(): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.shift + ApiUrl.poll + ApiUrl.vote,
    )
  }
}
