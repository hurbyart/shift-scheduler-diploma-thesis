import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiUrl} from "../constants/api.url";

@Injectable({
  providedIn: 'root',
})
export class UserService {
  CurrentUser: any;

  constructor(private http: HttpClient) {
  }

  getByRestaurant(): Observable<any> {
    return this.http.get(ApiUrl.baseUrl + ApiUrl.account + ApiUrl.restaurant, {responseType: 'json'});
  }

  getCurrent(): Observable<any> {
    return this.http.get(ApiUrl.baseUrl + ApiUrl.account + "/current", {responseType: 'json'})
  }
}
