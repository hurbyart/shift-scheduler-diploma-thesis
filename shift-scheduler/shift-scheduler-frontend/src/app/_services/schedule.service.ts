import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiUrl} from "../constants/api.url";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root',
})
export class ScheduleService {
  CurrentUser: any;

  constructor(private http: HttpClient) {
  }

  create(name: string, from: string, to: string): Observable<any> {
    console.log('from ' + from);
    console.log('to ' + to);
    return this.http.post(
      ApiUrl.baseUrl + ApiUrl.schedule,
      {name, from, to},
      httpOptions)
  }

  update(id: string, name: string, from: string, to: string): Observable<any> {
    return this.http.put(
      ApiUrl.baseUrl + ApiUrl.schedule,
      {id, name, from, to},
      httpOptions)
  }

  getAll(): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.schedule)
  }

  getById(id: String | null): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.schedule + '/' + id);
  }

  getByShiftId(id: String): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.schedule + ApiUrl.shift + '?shiftId=' + id);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(ApiUrl.baseUrl + ApiUrl.schedule + '/' + id);
  }

  calculateSchedule(id: string, bySalary: boolean): Observable<any> {
    return this.http.post(ApiUrl.baseUrl + ApiUrl.schedule + '/calculate/' + id + '?bySalary=' + bySalary,
      httpOptions);
  }
}
