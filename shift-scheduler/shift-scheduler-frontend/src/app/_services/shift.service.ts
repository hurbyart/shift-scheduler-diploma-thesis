import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiUrl} from "../constants/api.url";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root',
})
export class ShiftService {
  CurrentUser: any;

  constructor(private http: HttpClient) {
  }

  create(name: string, from: string, to: string, note: string, capacity: string, scheduleId: string | null): Observable<any> {
    return this.http.post(
      ApiUrl.baseUrl + ApiUrl.shift,
      {name, from, to, note, capacity, scheduleId},
      httpOptions)
  }

  get(shiftId: string | null): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + '/schedule/' + shiftId + ApiUrl.shift)
  }

  delete(id: string): Observable<any> {
    return this.http.delete(ApiUrl.baseUrl + ApiUrl.shift + '/' + id)
  }

  update(id: string, name: string, from: string, to: string, note: string, capacity: string, scheduleId: string | null): Observable<any> {
    console.log(scheduleId)
    return this.http.put(
      ApiUrl.baseUrl + ApiUrl.shift,
      {id, name, from, to, note, capacity, scheduleId},
      httpOptions)
  }


  getShiftsForCurrent(): Observable<any> {
    return this.http.get(
      ApiUrl.baseUrl + ApiUrl.shift
    )
  }
}
