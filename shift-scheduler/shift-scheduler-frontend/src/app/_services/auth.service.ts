import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiUrl} from "../constants/api.url";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(
      ApiUrl.baseUrl + ApiUrl.login,
      {
        username: username,
        password: password,
      },
      httpOptions
    );
  }

  register(username: string, password: string): Observable<any> {
    return this.http.post(
      ApiUrl.baseUrl + ApiUrl.user,
      {
        username,
        password,
      },
      httpOptions
    );
  }
}
