import {Component} from '@angular/core';
import {ShiftPollService} from "../_services/shift-poll.service";
import {ShiftPoll} from "../constants/shift-poll";
import {Employee} from "../constants/employee";
import {Shift} from "../constants/shift";
import {PollVoteService} from "../_services/poll-vote.service";
import {PollVote} from "../constants/poll-vote";
import {ScheduleService} from "../_services/schedule.service";
import {Schedule} from "../constants/schedule";

@Component({
  selector: 'app-shift-poll',
  templateUrl: './shift-poll.component.html',
  styleUrls: ['./shift-poll.component.css']
})
export class ShiftPollComponent {
  shiftPolls: ShiftPoll[] = [];
  pollVotes: { [key: string]: PollVote } = {};
  schedules: { [key: string]: Schedule } = {};

  constructor(private shiftPollService: ShiftPollService, private pollVoteService: PollVoteService,
              private scheduleService: ScheduleService ) {
    this.loadShiftPolls();
  }

  loadShiftPolls(): void {
    this.shiftPollService.getForCurrentAccount().subscribe({
      next: (data: ShiftPoll[]) => {
        this.shiftPolls = this.processShiftData(data);
        this.loadPollVotes();
        console.log(this.shiftPolls);
      },
      error: (error) => console.error('Error loading shift polls:', error)
    });
  }


  loadPollVotes(): void {
    for (const shiftPoll of this.shiftPolls) {
      this.pollVoteService.getByShiftPollId(shiftPoll.id).subscribe({
        next: (pollVote) => {
          this.pollVotes[shiftPoll.id] = pollVote[pollVote.length - 1];
          console.log('Poll Vote loaded for ShiftPoll ID:', shiftPoll.id);
          console.log(this.pollVotes);
        },
        error: (error) => {
          console.error('Error loading Poll Vote for ShiftPoll ID:', shiftPoll.id, error);
        }
      });
      this.scheduleService.getByShiftId(shiftPoll.shift.id).subscribe({
        next: (schedule) => {
          this.schedules[shiftPoll.id] = schedule;
        }
        }
      )
    }
  }


  processShiftData(shiftPolls: any[]): ShiftPoll[] {
    return shiftPolls.map(poll => {
      const from = new Date(poll.shift.from);
      const to = new Date(poll.shift.to);

      const updatedShift: Shift = {
        ...poll.shift,
        fromDate: from.toISOString().split('T')[0],
        fromTime: from.toTimeString().split(' ')[0].substring(0, 5),
        toDate: to.toISOString().split('T')[0],
        toTime: to.toTimeString().split(' ')[0].substring(0, 5),
      };

      return {
        id: poll.id,
        shift: updatedShift,
        account: poll.account
      };
    });
  }

  acceptShift(shiftPoll: ShiftPoll) {
    console.log('Accepted Shift ID:', shiftPoll);
    this.pollVoteService.create(true, shiftPoll).subscribe({
      next: () => {
        this.loadShiftPolls();
      },
      error: (error) => {
        console.error('Error creating shift:', error);
      }
    });
  }

  rejectShift(shiftPoll: ShiftPoll) {
    console.log('Rejected Shift ID:', shiftPoll);
    this.pollVoteService.create(false, shiftPoll).subscribe({
      next: () => {
        console.log(shiftPoll);
        this.loadShiftPolls();
      },
      error: (error) => {
        console.error('Error creating shift:', error);
      }
    });
  }

  changeAnswer(shiftPoll: ShiftPoll) {
    console.log('Change Answer for Shift ID:', shiftPoll.id);
    if(this.pollVotes[shiftPoll.id].answer) {
      this.rejectShift(shiftPoll);
    } else {
      this.acceptShift(shiftPoll);
    }
  }

  getPollVoteResult(shiftPollId: string): Boolean | null {
    const pollVote = this.pollVotes[shiftPollId];
    if(pollVote == undefined) return null;
    return pollVote ? pollVote.answer : null;
  }

  getPollScheduleResult(shiftPollId: string): Schedule {
    return this.schedules[shiftPollId];
  }

  formatRegisteredNames(registered: Employee[]): string {
    if (registered.length === 0) {
      return "---";
    }
    return registered.map(emp => `${emp.firstName} ${emp.secondName}`).join(', ');
  }
}
