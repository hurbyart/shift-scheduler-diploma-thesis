import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftPollComponent } from './shift-poll.component';

describe('ShiftPollComponent', () => {
  let component: ShiftPollComponent;
  let fixture: ComponentFixture<ShiftPollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShiftPollComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShiftPollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
