import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule} from '@angular/forms';
import {Shift} from "../constants/shift";
import {ShiftService} from "../_services/shift.service";
import {Schedule} from "../constants/schedule";
import {UserService} from "../_services/account.service";
import {ShiftPollService} from "../_services/shift-poll.service";
import {Employee} from "../constants/employee";
import {MatDatepickerInputEvent} from "@angular/material/datepicker";
import {ScheduleService} from "../_services/schedule.service";

@Component({
  selector: 'app-shift',
  templateUrl: './shift.component.html',
  styleUrls: ['./shift.component.css']
})
export class ShiftComponent {
  shiftForm: FormGroup;
  shifts: Shift[] = [];
  role: String;
  isShiftPollAlreadySent: { [key: string]: boolean } = {};
  locked: Boolean = false;
  schedule: Schedule | null = null;

  constructor(private shiftService: ShiftService, private fb: FormBuilder, private accountService: UserService,
              private shiftPollService: ShiftPollService, private scheduleService: ScheduleService) {
    this.shiftForm = this.fb.group({
      name: ['', Validators.required],
      fromDate: ['', Validators.required],
      fromTime: ['', Validators.required],
      toDate: ['', Validators.required],
      toTime: ['', Validators.required],
      capacity: ['', Validators.required],
      note: ['']
    });
    this.scheduleService.getById(window.sessionStorage.getItem("scheduleId")).subscribe({
        next: data => {
          this.schedule = this.processScheduleData(data);
        }
      }
    )
    this.role = "";
    accountService.getCurrent().subscribe({
      next: data => {
        this.role = data.role;
      }
    });

    this.getShifts();
  }

  getShifts(): any {
    this.shiftService.get(window.sessionStorage.getItem("scheduleId")).subscribe({
      next: data => {
        this.shifts = this.processShiftData(data).map(shift => ({...shift, isEditing: false}));
        this.updateShiftPollStatus(this.shifts);
      }
    });
  }

  submitForm(): void {
    if (this.shiftForm.valid) {
      const formValue = this.shiftForm.value;

      const fromDateTime = this.combineDateAndTime(formValue.fromDate, formValue.fromTime);
      const toDateTime = this.combineDateAndTime(formValue.toDate, formValue.toTime)


      this.shiftService.create(formValue.name, fromDateTime, toDateTime, formValue.note, formValue.capacity,
        window.sessionStorage.getItem("scheduleId")).subscribe({
        next: () => {
          this.shiftForm.reset();
          this.getShifts();
        },
        error: (error) => {
          console.error('Error creating shift:', error);
        }
      });
    } else {
      console.error('Form is not valid:', this.shiftForm.errors);
    }
  }

  deleteShift(id: string): any {
    this.shiftService.delete(id).subscribe({
      next: () => {
        this.getShifts();
      },
      error: (error) => {
        console.error('Error creating shift:', error);
      }
    })
  }

  onSubmit(): void {

  }

  combineDateAndTime(date: any, time: string): string {
    if (!(date instanceof Date)) {
      date = new Date(date);
    }

    const timeParts = time.split(':');
    date.setHours(parseInt(timeParts[0], 10));
    date.setMinutes(parseInt(timeParts[1], 10));

    return new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString();
  }

  processShiftData(shifts: any[]): Shift[] {
    return shifts.map(shift => {
      const from = new Date(shift.from);
      const to = new Date(shift.to);

      console.log(shift.registered);

      return {
        ...shift,
        fromDate: from.toISOString().split('T')[0],
        fromTime: from.toTimeString().split(' ')[0].substring(0, 5),
        toDate: to.toISOString().split('T')[0],
        toTime: to.toTimeString().split(' ')[0].substring(0, 5),
      };
    });
  }

  updateShift(shift: Shift): void {
    this.shiftService.update(shift.id, shift.name, this.combineDateAndTime(shift.fromDate, shift.fromTime),
      this.combineDateAndTime(shift.toDate, shift.toTime), shift.note, shift.capacity,
      window.sessionStorage.getItem("scheduleId")).subscribe({
      next: () => {
      },
      error: (error) => {
        console.error('Error creating shift:', error);
      }
    });
  }

  sendShiftPoll(shift: Shift): void {
    this.shiftPollService.send(shift).subscribe({
      next: () => {
        this.getShifts();
      },
      error: (error) => {
        console.error('Error creating shift:', error);
      }
    });
  }

  formatRegisteredNames(registered: Employee[]): string {
    return registered.map(emp => `${emp.firstName} ${emp.secondName}`).join(', ');
  }

  updateShiftPollStatus(shifts: Shift[]): void {
    shifts.forEach(shift => {
      this.shiftPollService.getByShift(shift.id).subscribe({
        next: data => {
          this.isShiftPollAlreadySent[shift.id] = data.length !== 0;
        },
        error: error => console.error('Error fetching shift poll status:', error)
      });
    });
  }

  onFromDateChange(event: MatDatepickerInputEvent<Date>) {
    const selectedDate = event.value;
    const toDateControl = this.shiftForm.get('toDate');

    if (toDateControl != null && selectedDate != null) {
      if (!toDateControl.value || toDateControl.value < selectedDate) {
        toDateControl.setValue(selectedDate);
      }
    }
  }

  processScheduleData(schedule: any): Schedule {
    const from = new Date(schedule.from);
    const to = new Date(schedule.to);
    console.log(from);
    console.log(to);

    return {
      ...schedule,
      fromDate: from.toISOString().split('T')[0],
      fromTime: from.toTimeString().split(' ')[0].substring(0, 5),
      toDate: to.toISOString().split('T')[0],
      toTime: to.toTimeString().split(' ')[0].substring(0, 5),
    };
  }

}
